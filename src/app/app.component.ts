import { Component } from '@angular/core';
import { Post } from './models/post';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'blog-med';

  posts =
  [ new Post("Mon premier post", "Milia ad peregrini ut indignitatis cum peregrini extrusis"+
   "peregrini indignitatis inpendio sectatoribus alimentorum interpellata dudum paucis"+
   "ventum saltatricum ab tempus pellerentur minimarum choris quidem quique haut remanerent ut ob cum"),
   new Post("Mon deuxième post", "Milia ad peregrini ut indignitatis cum peregrini extrusis"+
   "peregrini indignitatis inpendio sectatoribus alimentorum interpellata dudum paucis"+
   "ventum saltatricum ab tempus pellerentur minimarum choris quidem quique haut remanerent ut ob cum."),
   new Post("Encore un post", "Milia ad peregrini ut indignitatis cum peregrini extrusis"+
   "peregrini indignitatis inpendio sectatoribus alimentorum interpellata dudum paucis"+
   "ventum saltatricum ab tempus pellerentur minimarum choris quidem quique haut remanerent ut ob cum.")
  ] 

}
