import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-post-list-item-component',
  templateUrl: './post-list-item-component.component.html',
  styleUrls: ['./post-list-item-component.component.scss']
})
export class PostListItemComponentComponent implements OnInit {
  @Input() title: string;
  @Input() content: string;
  @Input() created_at: string;
  @Input() loveIts: number;

  constructor() { }

  ngOnInit() {
  }
  onAddLove(){
    this.loveIts++;
  }
  onDontLove(){
    this.loveIts--;
  }

  getLoveIts(){
    if(this.loveIts > 0)
    return true;
    else
    return false;
  }
}
